#!/bin/sh

origins=$(git remote show | cut -d_ -f2 )

if [[ -n $1 && "${origins[@]}" =~ $1 ]]; then
	git push --all origin_$1
elif [[ $1 == "--all" ]]; then
	for i in ${origins[@]}; do
		echo "Now pushing to $i..."
		git push --all origin_$i
	done
else
	echo "Usage: \"$0 <reponame>\" or to push to all origins one might want to use \"$0 --all\""
	echo "Possible reponames are:" 
	echo "${origins[@]}"
fi
